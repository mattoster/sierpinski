package org.oster.sierpinski.integration;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.oster.sierpinski.model.EquilateralTriangle;
import org.oster.sierpinski.model.SierpinskiTriangle;
import org.oster.sierpinski.model.Triangle;

public class MultiTriangleIntegrationTest {
    private TriangleIntegration testIntegration = new MultiTriangleIntegration();

    @Test
    public void testEquilateral() {
        Triangle test = testIntegration.getEquilateralTriangle(50, 50, 50);
        assertNotNull("Returned triangle should not be null.", test);
        assertTrue("Wrong instance of triangle", test instanceof EquilateralTriangle);
    }

    @Test
    public void testSierpinski() {
        Triangle test = testIntegration.getSierpinskiTriangle(50, 50, 50, 13);
        assertNotNull("Returned triangle should not be null.", test);
        assertTrue("Wrong instance of triangle", test instanceof SierpinskiTriangle);
    }

}
