package org.oster.sierpinski.service.model;

import static org.junit.Assert.assertNotNull;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;
import org.oster.sierpinski.service.TriangleService;

public class TriangleServiceTest extends JerseyTest {

    @Override
    protected Application configure() {
        return new ResourceConfig(TriangleService.class);
    }

    @Test
    public void testEquilateral() {
        String targetPath = "/triangle/equilateral/50/50/50";
        Response resp = target(targetPath).request().get();

        final DisplayTriangle triangle = resp.readEntity(DisplayTriangle.class);

        assertNotNull(triangle);
        assertNotNull(triangle.getPoints());
        assertNotNull(triangle.getPoints().get(0));
    }

    @Test
    public void testSierpinski() {
        String targetPath = "/triangle/sierpinski/50/50/50/1";
        Response resp = target(targetPath).request().get();

        final DisplaySierpinski triangle = resp.readEntity(DisplaySierpinski.class);

        assertNotNull(triangle);
        assertNotNull(triangle.getBase());
        assertNotNull(triangle.getTriangles().get(0));
    }

}
