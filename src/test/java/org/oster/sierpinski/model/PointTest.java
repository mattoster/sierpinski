package org.oster.sierpinski.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import org.junit.Test;

public class PointTest {
    private Point testPoint1 = new DoublePoint(1, 2);
    private Point testPoint2 = new DoublePoint(4, 6);

    private Point testPoint3 = new DoublePoint(200, 250);
    private Point testPoint4 = new DoublePoint(156.7, 175);
    private Point testPoint5 = new DoublePoint(243.3, 175);

    @Test
    public void testDistance() {
        // The distance between [1,2] and [4,6] should be 5
        double distance = testPoint1.getDistance(testPoint2);
        assertEquals("Expected distance not calculated within allowed delta.", 5, distance, .01);

        // The distances between the given points should be 86.6
        distance = testPoint3.getDistance(testPoint4);
        assertEquals("Expected distance not calculated within allowed delta.", 86.6, distance, .01);

        distance = testPoint4.getDistance(testPoint5);
        assertEquals("Expected distance not calculated within allowed delta.", 86.6, distance, .01);

        distance = testPoint5.getDistance(testPoint3);
        assertEquals("Expected distance not calculated within allowed delta.", 86.6, distance, .01);
    }

    @Test
    public void testMidPoint() {
        // The mid-point between these points should be [2.5,4]
        Point mid = testPoint1.getMidPoint(testPoint2);
        assertNotNull("midpoint should not be null", mid);
        assertEquals("Expected x incorrect and not within allowed delta.", 2.5, mid.getX(), .001);
        assertEquals("Expected y incorrect and not within allowed delta.", 4, mid.getY(), .001);
        assertTrue("Line does not contain the midpoint", isCCW(testPoint1, testPoint2, mid));

        // The mid-point between these points should be [200, 175]
        mid = testPoint4.getMidPoint(testPoint5);
        assertNotNull("midpoint should not be null", mid);
        assertEquals("Expected x incorrect and not within allowed delta.", 200, mid.getX(), .001);
        assertEquals("Expected y incorrect and not within allowed delta.", 175, mid.getY(), .001);
        assertTrue("Line does not contain the midpoint", isCCW(testPoint4, testPoint5, mid));

        // The mid-point between these points should be [178, 212.5]
        mid = testPoint4.getMidPoint(testPoint3);
        assertNotNull("midpoint should not be null", mid);
        assertEquals("Expected x incorrect and not within allowed delta.", 178.35, mid.getX(),
                     .001);
        assertEquals("Expected y incorrect and not within allowed delta.", 212.5, mid.getY(), .001);
        assertTrue("Line does not contain the midpoint", isCCW(testPoint4, testPoint3, mid));

        // The mid-point between these points should be [221.5, 212.5]
        mid = testPoint5.getMidPoint(testPoint3);
        assertNotNull("midpoint should not be null", mid);
        assertEquals("Expected x incorrect and not within allowed delta.", 221.65, mid.getX(),
                     .001);
        assertEquals("Expected y incorrect and not within allowed delta.", 212.5, mid.getY(), .001);
        assertTrue("Line does not contain the midpoint", isCCW(testPoint5, testPoint3, mid));
    }

    private boolean isCCW(Point p1, Point p2, Point mid) {
        // Is the midpoint is on the line between the two test points.
        Line2D line = new Line2D.Double(new Point2D.Double(p1.getX(), p1.getY()),
                                        new Point2D.Double(p2.getX(), p2.getY()));
        // This won't always return 0 due to rounding issues see relativeCCW doc
        return line.relativeCCW(new Point2D.Double(mid.getX(), mid.getY())) == 0;
    }

}
