package org.oster.sierpinski.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class TriangleTest {
    Point testCenter = new DoublePoint(50, 50);
    Triangle test = new EquilateralTriangle(testCenter, 50);

    @Test
    public void testArea() {
        // Perimeter of the triangle should be 3247.5
        double area = test.getArea();
        assertEquals("Area is not correct.", 3247.5, area, .1);
    }

    @Test
    public void testPerimeter() {
        // Perimeter of the triangle should be 259.8
        double perimiter = test.getPerimeter();
        assertEquals("Perimeter is not correct.", 259.8, perimiter, .01);
    }

    @Test
    public void testContains() {
        // Test center should definitely be in the triangle
        assertTrue("The point is not in the test triangle", test.contains(testCenter));

        // Test Points near the edges
        Point p1 = new DoublePoint(30, 60);
        assertTrue("The point is not in the test triangle", test.contains(p1));

        Point p2 = new DoublePoint(70, 60);
        assertTrue("The point is not in the test triangle", test.contains(p2));

        Point p3 = new DoublePoint(50, 26);
        assertTrue("The point is not in the test triangle", test.contains(p3));

        // Test points near the corners
        Point p4 = new DoublePoint(50, 99);
        assertTrue("The point is not in the test triangle", test.contains(p4));

        Point p5 = new DoublePoint(10, 26);
        assertTrue("The point is not in the test triangle", test.contains(p5));

        Point p6 = new DoublePoint(90, 26);
        assertTrue("The point is not in the test triangle", test.contains(p6));
    }

    @Test
    public void testDoesNotContain() {
        // Test Points near the edges
        Point p1 = new DoublePoint(30, 70);
        assertFalse("The point is not in the test triangle", test.contains(p1));

        Point p2 = new DoublePoint(80, 50);
        assertFalse("The point is not in the test triangle", test.contains(p2));

        Point p3 = new DoublePoint(50, 24);
        assertFalse("The point is not in the test triangle", test.contains(p3));

        // Test points near the corners
        Point p4 = new DoublePoint(50, 100.1);
        assertFalse("The point is not in the test triangle", test.contains(p4));

        Point p5 = new DoublePoint(6, 25);
        assertFalse("The point is not in the test triangle", test.contains(p5));

        Point p6 = new DoublePoint(94, 25);
        assertFalse("The point is not in the test triangle", test.contains(p6));
    }

}
