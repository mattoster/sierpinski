package org.oster.sierpinski.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class SierpinskiTriangleTest {
    private Triangle testBase = new EquilateralTriangle(new DoublePoint(50, 50), 50);

    @Test
    public void testFractalArea() {
        // For a Sierpinski with one fractal, area should be 75% of
        int testFractals = 1;
        Sierpinski test = new SierpinskiTriangle(testBase, testFractals);

        double factor = Math.pow(.75, testFractals);
        double expected = test.getBaseArea() * .75;
        double actual = test.getArea();

        assertEquals("The area should have been 75%", expected, actual, .1);

        testFractals = 2;
        test = new SierpinskiTriangle(testBase, testFractals);

        factor = Math.pow(.75, testFractals);
        expected = test.getBaseArea() * factor;
        actual = test.getArea();

        assertEquals("The area should have been 56.25%", expected, actual, .1);

        testFractals = 3;
        test = new SierpinskiTriangle(testBase, testFractals);

        factor = Math.pow(.75, testFractals);
        expected = test.getBaseArea() * factor;
        actual = test.getArea();

        assertEquals("The area should have been 42.18%", expected, actual, .1);
    }

    @Test
    public void testFractalCount() {
        // the number of remaining triangles should be 3 to the power of fractals
        int testFractals = 3;
        Sierpinski test = new SierpinskiTriangle(testBase, testFractals);

        int expected = (int) Math.pow(3, testFractals);
        int actual = test.getRemainingTriangleCount();

        assertEquals("The count should have been 27", expected, actual);

        testFractals = 5;
        test = new SierpinskiTriangle(testBase, testFractals);

        expected = (int) Math.pow(3, testFractals);
        actual = test.getRemainingTriangleCount();

        assertEquals("The count should have been 243", expected, actual);
    }

    @Test
    public void testMaxFractalCount() {
        // the number of remaining triangles should be 3 to the power of fractals
        int testFractals = SierpinskiTriangle.getMaxFractal();
        Sierpinski test = new SierpinskiTriangle(testBase, testFractals);

        int expected = (int) Math.pow(3, testFractals);
        int actual = test.getRemainingTriangleCount();

        assertEquals("The count should have been " + expected, expected, actual);
    }

    @Test
    public void testContains() {
        // Corners and edges are good candidates
        int testFractals = 3;
        Sierpinski test = new SierpinskiTriangle(testBase, testFractals);

        Point p1 = new DoublePoint(10, 30);
        assertTrue("Point is not within the remaining triangles.", test.contains(p1));

        Point p2 = new DoublePoint(25, 30);
        assertTrue("Point is not within the remaining triangles.", test.contains(p2));

        Point p3 = new DoublePoint(90, 30);
        assertTrue("Point is not within the remaining triangles.", test.contains(p3));

        Point p4 = new DoublePoint(50, 95);
        assertTrue("Point is not within the remaining triangles.", test.contains(p4));
    }

    @Test
    public void testDoesNotContain() {
        // Corners and edges are good candidates
        int testFractals = 3;
        Sierpinski test = new SierpinskiTriangle(testBase, testFractals);

        // Center point definitely should not be in a remaining area
        Point p1 = new DoublePoint(0, 0);
        assertFalse("Point is not within the remaining triangles.", test.contains(p1));

        Point p2 = new DoublePoint(50, 70);
        assertFalse("Point is not within the remaining triangles.", test.contains(p2));

        Point p3 = new DoublePoint(30, 40);
        assertFalse("Point is not within the remaining triangles.", test.contains(p3));

        Point p4 = new DoublePoint(35, 71);
        assertFalse("Point is not within the remaining triangles.", test.contains(p4));
    }

}
