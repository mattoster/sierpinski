var triangleClient = {
        api: "http://localhost:8080/sierpinski/api/triangle/sierpinski/",
        defaultFractals: 2,
        defaultRadius: 100,
        zoom: {
            current: 100,
            factor: 1,
            fractalStep: 300,
            increment: 50,
            fractals: 2,
            maxFractals: 8 // Selected for client performance reasons
        },
        canvas: triangleCanvas,
        canvasContext: null,
        currentTriangle: null,
        lastX: null,
        lastY: null,
        loadTriangle: function(url, callback){
            // Make a server-side request for triangle details
            var client = new XMLHttpRequest();
            // asynchronous at high fractals and zooms causes issues.
            client.open("GET", url, false);
            client.setRequestHeader("accept", "application/json");
            client.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    triangleClient.currentTriangle = JSON.parse(this.responseText);
                    callback(triangleClient.currentTriangle);
                }
            };
            client.send(null);
        },
        loadDefaultTriangle: function() {
            // Load the default triangle that starts on the screen
            triangleClient.canvas["createContext"]();
            var fractalStat = document.getElementById("fractalCount");
            var zoomStat = document.getElementById("zoomLevel");
            var triangleStat = document.getElementById("triangleCount");
            var cp = triangleClient.canvas["getCanvasCenter"]();
            var rad = triangleClient.defaultRadius;
            var frac = triangleClient.defaultFractals;
            var url = triangleClient.api + cp.x + "/" + cp.y + "/" + rad + "/" + frac;
            triangleClient["loadTriangle"](url, triangleClient.canvas["drawSierpinski"]);
            fractalStat.innerHTML = triangleClient.zoom.fractals;
            zoomStat.innerHTML = triangleClient.zoom.current;
            triangleStat.innerHTML = triangleClient.currentTriangle.triangles.length;
        },
        calculateZoomAndFractals: function(triangle, isZoomIn) {
            // Adjust the zoom and return the fractals
            var zoom = triangleClient.zoom;
            var defRadius = triangleClient.defaultRadius;
            var fractal = null;
            var isZoomed = false;
            if(isZoomIn) {
                // If we have reached max fractals, no more zooming
                if(triangle.fractals < zoom.maxFractals) {
                    zoom.current = zoom.current + zoom.increment;
                    zoom.factor = zoom.current / 100;
                    triangle.base.radius = defRadius * zoom.factor;
                    if (zoom.current % zoom.fractalStep == 0) {
                        fractal = triangle.fractals + 1;
                    }
                    else {
                        fractal = triangle.fractals;
                    }
                    isZoomed = true;
                }
            }
            else {
                if (zoom.current > 100) {
                    if(zoom.current % zoom.fractalStep == 0 && triangle.fractals > 1) {
                        fractal = triangle.fractals - 1;
                    }
                    else {
                        fractal = triangle.fractals;
                    }
                    zoom.current = zoom.current - zoom.increment;
                    zoom.factor = zoom.current / 100;
                    triangle.base.radius = defRadius * zoom.factor;
                    isZoomed = true;
                }
            }
            if(isZoomed) {
                var tmp = fractal ? fractal : zoom.fractals;
                zoom.fractals = tmp ? tmp : triangleClient.defaultFractals;
            }
            triangleClient["updateStats"]();
            return fractal;
        },
        updateStats: function() {
            var fractalStat = document.getElementById("fractalCount");
            var zoomStat = document.getElementById("zoomLevel");
            var triangleStat = document.getElementById("triangleCount");
            fractalStat.innerHTML = triangleClient.zoom.fractals;
            zoomStat.innerHTML = triangleClient.zoom.current;
            triangleStat.innerHTML = triangleClient.currentTriangle.triangles.length;
        }
};