
triangleCanvas = {
    name: "triangleCanvas",
    dragStart: null,
    dragged: null,
    dragStartCenter: null,
    drawSierpinski: function(triangle) {
        // This function draws the triangles that make up the Sierpinski triangle.
        for(var i = 0; i < triangle.triangles.length; i++) {
            var drawMe = triangle.triangles[i];
            triangleCanvas["drawTriangle"](drawMe);
        }
    },
    drawTriangle: function(triangle) {
        // This function draws a triangle on the canvas and then fills it in.
        var ctx = triangleCanvas["getCanvasContext"]();
        var points = triangle.points;
        var p1 = triangle.points[0];
        var p2 = triangle.points[1];
        var p3 = triangle.points[2];
        
        ctx.beginPath();
        ctx.moveTo(p1.x, p1.y);
        ctx.lineTo(p2.x, p2.y);
        ctx.lineTo(p3.x, p3.y);
        ctx.fill();
    },
    redrawSierpinski: function(triangle) {
        triangleCanvas["resetContext"]();
        triangleCanvas["drawSierpinski"](triangle);
    },
    redrawTriangle: function(triangle) {
        triangleCanvas["resetContext"]();
        triangleCanvas["drawTriangle"](triangle);
    },
    handleScroll: function(evt) {
        var delta = evt.wheelDelta ? evt.wheelDelta/40 : evt.detail ? -evt.detail : 0;
        var zoomIn = delta >= 0 ? true : false;
        var frac = triangleClient["calculateZoomAndFractals"](triangleClient.currentTriangle, zoomIn);
        if(frac) {
            var cp = triangleClient.currentTriangle.base.center;
            var rad = triangleClient.currentTriangle.base.radius;
            var url = triangleClient.api + cp.x + "/" + cp.y + "/" + rad + "/" + frac;
            triangleClient["loadTriangle"](url, triangleCanvas["redrawSierpinski"]);
        }
        return evt.preventDefault() && false;
    },
    mousedown: function(evt){
        var ctx = triangleCanvas["getCanvasContext"]();
        var canvas = ctx.canvas;
        var lastX=triangleCanvas["getCanvasCenter"]().x, lastY=triangleCanvas["getCanvasCenter"]().y;
        document.body.style.mozUserSelect = document.body.style.webkitUserSelect = document.body.style.userSelect = 'none';
        lastX = evt.offsetX || (evt.pageX - canvas.offsetLeft);
        lastY = evt.offsetY || (evt.pageY - canvas.offsetTop);
        triangleCanvas.dragStart = {x: lastX, y: lastY};
        triangleCanvas.dragStartCenter = {x: triangleClient.currentTriangle.base.center.x,
                                          y: triangleClient.currentTriangle.base.center.y};
        triangleCanvas.dragged = false;
    },
    mousemove: function(evt) {
        var ctx = triangleCanvas["getCanvasContext"]();
        var canvas = ctx.canvas;
        var lastX = triangleClient.lastX;
        var lastY = triangleClient.lastY;
        lastX = lastX ? lastX : triangleCanvas["getCanvasCenter"]().x;
        lastY = lastY ? lastY : triangleCanvas["getCanvasCenter"]().y;
        lastX = evt.offsetX || (evt.pageX - canvas.offsetLeft);
        lastY = evt.offsetY || (evt.pageY - canvas.offsetTop);
        triangleCanvas.dragged = true;
        if (triangleCanvas.dragStart){
            var diff = {x: lastX - triangleCanvas.dragStart.x, y: lastY - triangleCanvas.dragStart.y};
            var triangle = triangleClient.currentTriangle;
            var cp = {x: triangleCanvas.dragStartCenter.x + diff.x, y: triangleCanvas.dragStartCenter.y - diff.y};
            var rad = triangle.base.radius;
            var frac = triangle.fractals;
            var url = triangleClient.api + cp.x + "/" + cp.y + "/" + rad + "/" + frac;
            triangleClient["loadTriangle"](url, triangleCanvas["redrawSierpinski"]);
        }
    },
    mouseup: function(evt){
        triangleCanvas.dragStart = null;
    },
    createContext: function() {
        var ctx = triangleCanvas["getCanvasContext"]();
        var h = ctx.canvas.height;
        // flip the y axis
        ctx.transform(1, 0, 0, -1, 0, h);
        ctx.canvas.addEventListener('DOMMouseScroll', triangleCanvas["handleScroll"], false);
        ctx.canvas.addEventListener('mousewheel', triangleCanvas["handleScroll"], false);
        
        ctx.canvas.addEventListener('mousedown', triangleCanvas["mousedown"], false);
        ctx.canvas.addEventListener('mousemove', triangleCanvas["mousemove"], false);
        ctx.canvas.addEventListener('mouseup', triangleCanvas["mouseup"], false);
    },
    resetContext: function() {
        var ctx = triangleCanvas["getCanvasContext"]();
        ctx.save();
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        ctx.restore();
    },
    getCanvasContext: function() {
        return document.getElementById(triangleCanvas.name).getContext("2d");
    },
    getCanvasCenter: function() {
        var ctx = triangleCanvas["getCanvasContext"]();
        return {x: ctx.canvas.width / 2, y: ctx.canvas.height / 2};
    }
}
