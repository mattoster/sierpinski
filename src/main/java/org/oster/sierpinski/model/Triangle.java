package org.oster.sierpinski.model;

public interface Triangle {

    public Point getA();

    public Point getB();

    public Point getC();

    public Point getCenter();

    public double getRadius();

    public double getArea();

    public double getPerimeter();

    public boolean contains(Point p);

}
