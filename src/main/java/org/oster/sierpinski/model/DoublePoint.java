package org.oster.sierpinski.model;

import java.awt.geom.Point2D;
import java.util.Objects;

public class DoublePoint extends Point2D.Double implements Point {
    private static final long serialVersionUID = 5802739677185670141L;

    public DoublePoint(double x, double y) {
        super(x, y);
    }

    @Override
    public double getDistance(Point other) {
        double dSq = super.distanceSq((Point2D.Double) other);
        return Math.sqrt(dSq);
    }

    @Override
    public Point getMidPoint(Point other) {
        double midX = (this.getX() + other.getX()) / 2;
        double midY = (this.getY() + other.getY()) / 2;
        return new DoublePoint(midX, midY);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (! (o instanceof DoublePoint)) {
            return false;
        }
        DoublePoint p = (DoublePoint) o;
        return Objects.equals(getX(), p.getX()) && Objects.equals(getY(), p.getY());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getX(), getY());
    }

    @Override
    public String toString() {
        return String.format("{x:%s, y:%s}", getX(), getY());
    }

}
