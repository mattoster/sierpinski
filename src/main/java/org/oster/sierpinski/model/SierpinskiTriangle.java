package org.oster.sierpinski.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SierpinskiTriangle implements Sierpinski {
    private static final int MAX_FRACTAL = 13;
    private int fractals;
    private Triangle base;
    private List<Triangle> remaining = new ArrayList<>();

    public SierpinskiTriangle(Triangle base, int fractals) {
        this.fractals = fractals < MAX_FRACTAL ? fractals : MAX_FRACTAL;
        this.base = base;
        calculateSierpinski(this, fractals);
    }

    public SierpinskiTriangle(double x, double y, double radius, int fractals) {
        this.fractals = fractals < MAX_FRACTAL ? fractals : MAX_FRACTAL;
        this.base = new EquilateralTriangle(new DoublePoint(x, y), radius);
        calculateSierpinski(this, fractals);
    }

    public static int getMaxFractal() {
        return MAX_FRACTAL;
    }

    @Override
    public Triangle getBaseTriangle() {
        return base;
    }

    @Override
    public List<Triangle> getRemainingTriangles() {
        return remaining;
    }

    @Override
    public Point getA() {
        return base.getA();
    }

    @Override
    public Point getB() {
        return base.getB();
    }

    @Override
    public Point getC() {
        return base.getC();
    }

    @Override
    public Point getCenter() {
        return base.getCenter();
    }

    @Override
    public double getRadius() {
        return base.getRadius();
    }

    @Override
    public int getFractals() {
        return fractals;
    }

    @Override
    public int getRemainingTriangleCount() {
        return remaining != null ? remaining.size() : 0;
    }

    @Override
    public double getBaseArea() {
        return base.getArea();
    }

    /**
     * Determine the area of the remaining triangles of this Sierpinski triangle. Calculated using
     * barycentric coordinates of the point being passed in.
     * 
     * @return
     */
    @Override
    public double getArea() {
        double sum = 0;
        if (remaining != null) {
            for (Triangle t : remaining) {
                sum += t.getArea();
            }
        }
        return sum;
    }

    @Override
    public double getPerimeter() {
        return base.getPerimeter();
    }

    /**
     * Determine if a Point is first within the outer triangle, if it is, determine if the point is
     * in any of the remaining triangles of this Sierpinski triangle. Calculated using barycentric
     * coordinates of the point being passed in.
     * 
     * @param point
     * @return
     */
    @Override
    public boolean contains(Point p) {
        boolean isInSierpinski = base.contains(p);
        if (isInSierpinski) {
            isInSierpinski = false;
            for (Triangle t : remaining) {
                isInSierpinski = t.contains(p) == true ? true : isInSierpinski;
            }
        }
        return isInSierpinski;
    }

    /**
     * Calculate the remaining triangles in this Sierplinski triangle base on the number of fractals
     * to apply. This is a recursive function which
     * 
     * @param triangle
     * @param fractals void
     */
    private void calculateSierpinski(Triangle triangle, int fractals) {
        if (fractals > 0) {
            Triangle top = new EquilateralTriangle(triangle.getA(),
                                                   triangle.getB().getMidPoint(triangle.getA()),
                                                   triangle.getC().getMidPoint(triangle.getA()));
            calculateSierpinski(top, fractals - 1);

            Triangle left = new EquilateralTriangle(triangle.getA().getMidPoint(triangle.getB()),
                                                    triangle.getB(),
                                                    triangle.getC().getMidPoint(triangle.getB()));
            calculateSierpinski(left, fractals - 1);

            Triangle right = new EquilateralTriangle(triangle.getA().getMidPoint(triangle.getC()),
                                                     triangle.getB().getMidPoint(triangle.getC()),
                                                     triangle.getC());
            calculateSierpinski(right, fractals - 1);
        }
        else {
            remaining.add(triangle);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (! (o instanceof SierpinskiTriangle)) {
            return false;
        }
        SierpinskiTriangle triangle = (SierpinskiTriangle) o;
        return Objects.equals(getBaseTriangle(), triangle.getBaseTriangle()) &&
               fractals == triangle.getFractals();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBaseTriangle(), fractals);
    }

    @Override
    public String toString() {
        return String.format("{base: %s, fractal: %d}", getBaseTriangle(), fractals);
    }

}
