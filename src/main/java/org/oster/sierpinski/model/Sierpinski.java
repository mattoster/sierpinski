package org.oster.sierpinski.model;

import java.util.List;

public interface Sierpinski extends Triangle {

    public double getBaseArea();

    public Triangle getBaseTriangle();

    public int getFractals();

    public int getRemainingTriangleCount();

    public List<Triangle> getRemainingTriangles();

}
