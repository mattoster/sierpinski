package org.oster.sierpinski.model;

public interface Point {

    public double getX();

    public double getY();

    /**
     * Returns the distance between this point and the point provided as a parameter.
     * 
     * @param other
     * @return
     */
    public double getDistance(Point other);

    /**
     * Returns the mid-point on the line between this point and the point provided as a parameter.
     * 
     * @param p1
     * @param p2
     * @return
     */
    public Point getMidPoint(Point other);

}
