package org.oster.sierpinski.model;

import java.util.Objects;

public class EquilateralTriangle implements Triangle {
    private Point a;
    private Point b;
    private Point c;

    private Point center;
    private double radius;

    /**
     * Create an EquilateralTriangle from a center point and a radius of the circle to draw the
     * triangle within.
     * 
     * @param center
     * @param radius
     */
    public EquilateralTriangle(Point center, double radius) {
        this.center = center;
        this.radius = radius;
        this.init();
    }

    // This constructor allows for the possibility of creating a triangle that isn't equilateral
    public EquilateralTriangle(Point a, Point b, Point c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    private void init() {
        this.a = new DoublePoint(center.getX(), center.getY() + radius);

        double tmpX = a.getX() - center.getX();
        double tmpY = a.getY() - center.getY();

        double cos120 = Math.cos(Math.toRadians(120));
        double sin120 = Math.sin(Math.toRadians(120));
        double cos240 = Math.cos(Math.toRadians(240));
        double sin240 = Math.sin(Math.toRadians(240));

        double bx = ( (tmpX * cos120) - (tmpY * sin120)) + center.getX();
        double by = ( (tmpX * sin120) + (tmpY * cos120)) + center.getY();
        this.b = new DoublePoint(bx, by);

        double cx = ( (tmpX * cos240) - (tmpY * sin240)) + center.getX();
        double cy = ( (tmpX * sin240) + (tmpY * cos240)) + center.getY();
        this.c = new DoublePoint(cx, cy);
    }

    @Override
    public Point getA() {
        return a;
    }

    @Override
    public Point getB() {
        return b;
    }

    @Override
    public Point getC() {
        return c;
    }

    @Override
    public Point getCenter() {
        return center;
    }

    @Override
    public double getRadius() {
        return radius;
    }

    @Override
    public double getArea() {
        double side = getPerimeter() / 3;
        return (Math.sqrt(3) / 4) * Math.pow(side, 2);
    }

    @Override
    public double getPerimeter() {
        return a.getDistance(b) + b.getDistance(c) + c.getDistance(a);
    }

    /**
     * Determine if a Point is within this triangle. Calculated using barycentric coordinates of the
     * point being passed in.
     * 
     * @param point
     * @return
     */
    @Override
    public boolean contains(Point p) {
        double alpha = ( (b.getY() - c.getY()) * (p.getX() - c.getX()) +
                         (c.getX() - b.getX()) * (p.getY() - c.getY())) /
                       ( (b.getY() - c.getY()) * (a.getX() - c.getX()) +
                         (c.getX() - b.getX()) * (a.getY() - c.getY()));
        double beta = ( (c.getY() - a.getY()) * (p.getX() - c.getX()) +
                        (a.getX() - c.getX()) * (p.getY() - c.getY())) /
                      ( (b.getY() - c.getY()) * (a.getX() - c.getX()) +
                        (c.getX() - b.getX()) * (a.getY() - c.getY()));
        double gamma = 1.0 - alpha - beta;

        return alpha > 0 && beta > 0 && gamma > 0;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (! (o instanceof EquilateralTriangle)) {
            return false;
        }
        EquilateralTriangle eq = (EquilateralTriangle) o;
        return Objects.equals(getA(), eq.getA()) && Objects.equals(getB(), eq.getB()) &&
               Objects.equals(getC(), eq.getC()) && Objects.equals(getCenter(), eq.getCenter()) &&
               radius == eq.getRadius();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getA(), getB(), getC(), getCenter(), radius);
    }

    @Override
    public String toString() {
        return String.format("{A:%s, B:%s, C:%s}", getA(), getB(), getC());
    }

}
