package org.oster.sierpinski.service.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.oster.sierpinski.model.Triangle;

@XmlRootElement(name = "triangle")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class DisplayTriangle {

    @XmlElement
    private DisplayPoint center;

    @XmlAttribute
    double radius;

    @XmlElement
    private List<DisplayPoint> points;

    public DisplayTriangle() {
        
    }

    public DisplayTriangle(Triangle triangle) {
        if (triangle.getCenter() != null) {
            this.center = new DisplayPoint(triangle.getCenter());
            this.radius = triangle.getRadius();
        }
        points = new ArrayList<>();
        points.add(new DisplayPoint(triangle.getA()));
        points.add(new DisplayPoint(triangle.getB()));
        points.add(new DisplayPoint(triangle.getC()));
    }

    public DisplayPoint getCenter() {
        return center;
    }

    public double getRadius() {
        return radius;
    }

    public List<DisplayPoint> getPoints() {
        return points;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (! (o instanceof DisplayTriangle)) {
            return false;
        }
        DisplayTriangle eq = (DisplayTriangle) o;
        return Objects.equals(getCenter(), eq.getCenter()) &&
               Objects.equals(getPoints(), eq.getPoints()) && radius == eq.getRadius();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCenter(), getPoints(), radius);
    }

    @Override
    public String toString() {
        return String.format("{center: %s, radius: %d, points:[%s]}", getCenter(), getRadius(),
                             getPoints());
    }
}
