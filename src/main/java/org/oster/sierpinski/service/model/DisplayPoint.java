package org.oster.sierpinski.service.model;

import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import org.oster.sierpinski.model.Point;

@XmlRootElement(name = "point")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class DisplayPoint {
    @XmlAttribute
    private double x;

    @XmlAttribute
    private double y;

    public DisplayPoint() {

    }

    public DisplayPoint(Point p) {
        this.x = p.getX();
        this.y = p.getY();
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (! (o instanceof DisplayPoint)) {
            return false;
        }
        DisplayPoint point = (DisplayPoint) o;
        return getX() == point.getX() && getY() == point.getY();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getX(), getY());
    }

    @Override
    public String toString() {
        return String.format("{x: %s, y: %s}", getX(), getY());
    }

}
