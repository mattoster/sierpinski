package org.oster.sierpinski.service.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.oster.sierpinski.model.Sierpinski;
import org.oster.sierpinski.model.Triangle;

@XmlRootElement(name = "sierpinski")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class DisplaySierpinski {
    @XmlAttribute
    private int fractals;

    @XmlElement
    private DisplayTriangle base;

    @XmlElement
    private List<DisplayTriangle> triangles;

    public DisplaySierpinski() {

    }

    public DisplaySierpinski(Sierpinski triangle) {
        triangles = new ArrayList<>();
        this.fractals = triangle.getFractals();
        this.base = new DisplayTriangle(triangle.getBaseTriangle());
        for (Triangle t : triangle.getRemainingTriangles()) {
            DisplayTriangle toAdd = new DisplayTriangle(t);
            this.triangles.add(toAdd);
        }
    }

    public int getFractals() {
        return fractals;
    }

    public DisplayTriangle getBase() {
        return base;
    }

    public List<DisplayTriangle> getTriangles() {
        return triangles;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (! (o instanceof DisplaySierpinski)) {
            return false;
        }
        DisplaySierpinski triangle = (DisplaySierpinski) o;
        return Objects.equals(getBase(), triangle.getBase()) &&
               Objects.equals(getTriangles(), triangle.getTriangles());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBase(), getTriangles());
    }

    @Override
    public String toString() {
        return String.format("{base: %s, triangles:[%s]}", getBase(), getTriangles());
    }

}
