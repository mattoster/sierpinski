package org.oster.sierpinski.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.oster.sierpinski.integration.MultiTriangleIntegration;
import org.oster.sierpinski.integration.TriangleIntegration;
import org.oster.sierpinski.model.Sierpinski;
import org.oster.sierpinski.model.Triangle;
import org.oster.sierpinski.service.model.DisplaySierpinski;
import org.oster.sierpinski.service.model.DisplayTriangle;

@Path("/triangle")
public class TriangleService {
    // If dependency injection was being used, the concrete class wouldn't be known and decoupled
    // from this class.
    private TriangleIntegration integration = new MultiTriangleIntegration();

    @Path("/equilateral/{x}/{y}/{r}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getEquilateralTriangle(@PathParam("x") double x, @PathParam("y") double y,
                                           @PathParam("r") double r) {
        Response resp = null;
        Triangle eq = integration.getEquilateralTriangle(x, y, r);

        if (eq == null) {
            String msg = "An equilateral triangle could not be created.";
            resp = Response.serverError().entity(msg).type(MediaType.TEXT_PLAIN).build();
        }
        else {
            DisplayTriangle disp = new DisplayTriangle(eq);
            resp = Response.ok().entity(disp).type(MediaType.APPLICATION_JSON).build();
        }
        return resp;
    }

    @Path("/sierpinski/{x}/{y}/{r}/{f}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getSierpinskiTriangle(@PathParam("x") double x, @PathParam("y") double y,
                                          @PathParam("r") double r, @PathParam("f") int f) {
        Response resp = null;
        Sierpinski sierpinski = integration.getSierpinskiTriangle(x, y, r, f);

        if (sierpinski == null) {
            String msg = "A Sierpinski triangle could not be created.";
            resp = Response.serverError().entity(msg).type(MediaType.TEXT_PLAIN).build();
        }
        else {
            DisplaySierpinski disp = new DisplaySierpinski(sierpinski);
            resp = Response.ok().entity(disp).type(MediaType.APPLICATION_JSON).build();
        }
        return resp;
    }

}
