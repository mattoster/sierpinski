package org.oster.sierpinski.integration;

import org.oster.sierpinski.model.DoublePoint;
import org.oster.sierpinski.model.EquilateralTriangle;
import org.oster.sierpinski.model.Point;
import org.oster.sierpinski.model.Sierpinski;
import org.oster.sierpinski.model.SierpinskiTriangle;
import org.oster.sierpinski.model.Triangle;

/**
 * Implementation of application integration logic.
 * 
 * @author moster
 */
public class MultiTriangleIntegration implements TriangleIntegration {

    @Override
    public Triangle getEquilateralTriangle(double x, double y, double radius) {
        Point center = new DoublePoint(x, y);
        Triangle eq = new EquilateralTriangle(center, radius);
        return eq;
    }

    @Override
    public Sierpinski getSierpinskiTriangle(double x, double y, double radius, int fractals) {
        Point center = new DoublePoint(x, y);
        Triangle base = new EquilateralTriangle(center, radius);
        Sierpinski sierpinski = new SierpinskiTriangle(base, fractals);
        return sierpinski;
    }

}
