package org.oster.sierpinski.integration;

import org.oster.sierpinski.model.Sierpinski;
import org.oster.sierpinski.model.Triangle;

/**
 * Interface for integrating between external web services and internal application logic.
 * 
 * @author moster
 */
public interface TriangleIntegration {

    public Triangle getEquilateralTriangle(double x, double y, double radius);

    public Sierpinski getSierpinskiTriangle(double x, double y, double radius, int fractals);

}
